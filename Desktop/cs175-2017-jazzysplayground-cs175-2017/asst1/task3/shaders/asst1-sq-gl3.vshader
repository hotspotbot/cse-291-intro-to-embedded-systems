#version 150
uniform float uVertexScale;
uniform float uVertexScaleX;
uniform float uVertexScaleY;

in vec2 aPosition;
in vec2 aTexCoord;

out vec2 vTexCoord;
out vec2 vTemp;

void main() {
	gl_Position = vec4(aPosition.x * uVertexScale * uVertexScaleX, aPosition.y * uVertexScaleY, 0, 1);
  vTexCoord = aTexCoord;
  vTemp = vec2(1, 1);
}
