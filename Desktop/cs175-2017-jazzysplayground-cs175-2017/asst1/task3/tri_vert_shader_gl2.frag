uniform sampler2D uTex0;

varying vec2 vTexCoord;
varying vec3 vColor;

void main(void) {
  // The texture(..) calls always return a vec4. Data comes out of a texture in RGBA format
  vec4 texColor0 = texture2D(uTex0, vTexCoord);

  // fragColor is a vec4. The components are interpreted as red, green, blue, and alpha
  gl_FragColor = vec4(vColor, 1.0) * texColor0;
}
