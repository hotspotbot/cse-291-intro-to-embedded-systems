uniform float uVertexScale;
uniform float uVertexScaleX;
uniform float uVertexScaleY;
uniform vec2 uPosOffset;

attribute vec2 aPosition;
attribute vec2 aTexCoord;
attribute vec3 aColor;

varying vec2 vTexCoord;
varying vec3 vColor;

void main() {
  gl_Position = vec4((aPosition.x * uVertexScale * uVertexScaleX) + uPosOffset.x, (aPosition.y * uVertexScaleY) + uPosOffset.y, 0, 1);
  vTexCoord = aTexCoord;
  vColor = aColor;
}
