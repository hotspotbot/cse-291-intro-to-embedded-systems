#version 150
uniform float uVertexScale;
uniform float uVertexScaleX;
uniform float uVertexScaleY;

uniform vec2 uPosOffset;

in vec2 aPosition;
in vec2 aTexCoord;
in vec3 aColor;

out vec2 vTexCoord;
out vec3 vColor;

void main() {
  gl_Position = vec4((aPosition.x * uVertexScale * uVertexScaleX) + uPosOffset.x, (aPosition.y * uVertexScaleY) + uPosOffset.y, 0, 1);
  vTexCoord = aTexCoord;
  vColor = aColor;
}
