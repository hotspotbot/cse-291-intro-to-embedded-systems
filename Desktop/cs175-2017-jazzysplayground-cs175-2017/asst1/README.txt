Tasks were developed using Windows, and Visual Studio 2015 to compile them.

To compile open the files .vcxproj on both folders or open the solution files task2.sln and task4.sln respectively.

All requirements were performed properly; the base code runs without bugs. 

Taks2
The idea behind the transformation is to scale the vertexes directly on the vertex shader. For that, the proportions between the complete output window and the spatial position of the quadrilateral were computed.

Task 3
This task required more details due that loading the shader for a triangle was implemented. Also, a new way to define a geometry was defined in order to load and display using this own shaders. At the same time, some parameters were passed in order to move triangle using keyboard.

In both tasks, using the right-click of mouse it is possible scale in X-axis the object. Also, both are properly scaled when the window is resized. As original implementation, using ‘q’ exits from the program, ‘s’ stores an image called ‘out.ppm’, and ‘h’ shows a little help. In task #4, there are additional keys: ‘i’ moves up the triangle, ‘k’ moves down the triangle, ‘j’ moves left the triangle, ‘l’ moves right the triangle,
